function [] = soldering_tip_linear_reg()
    clear; clc; clf;

    data = csvread("szf-tip1-calib1.txt", 1, 0);

    x = data(:, 1);
    x *= 3300 / 4095 * 1000 / 221;
    y = data(:, 2);

    plot(x, y, "bs", "markersize", 5, "marker", "+");

    %x_range = [0 4095];
    %x_range = [1200 3000];
    %xlim(x_range);
    title("Soldering Tip Calibration");
    %xlabel("ADC value");
    xlabel("Tip Voltage (µV)");
    ylabel("Temperature (°C)");
    hold on;
    grid minor;

    xq = linspace(min(x), max(x), 1000);
    p = polyfit(x, y, 1)
    plot(xq, polyval(p,xq), "-", "color", "red");

    %print("plot.png", "-dpng")
end

