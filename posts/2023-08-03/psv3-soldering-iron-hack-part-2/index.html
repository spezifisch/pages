<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PSV3 Soldering Iron Hack - Part 2</title>
    <meta name="description" content="Reverse engineering and implementing a replacement firmware for a cheap soldering iron.">
    <link rel="shortcut icon" href="/img/favicon-32x32.png" sizes="32x32">
    <link rel="apple-touch-icon" href="/img/apple-touch-icon.png">

    <style>
        /* prevent FOUC (reverted in index.css) */
        body {
            visibility: hidden;
            opacity: 0;
        }
    </style>
    <link rel="stylesheet" href="/css/index.css">
    <link rel="stylesheet" href="/css/prism-base16-monokai.dark.css">
    <link rel="stylesheet" href="/css/prism-diff.css">
    <link rel="alternate" href="/feed/feed.xml" type="application/atom+xml" title="spezifisches">
    <link rel="alternate" href="/feed/feed.json" type="application/json" title="spezifisches">
  </head>
  <body>
    <header>
      <p class="home">
          <a href="/"><img src="/img/spezifish512.png" width=32 height=32 alt="" /> <span>spezifisches</span></a>
      </p>
      <ul class="nav">
          <li class="nav-item">
            <a href="/">Home</a>
          </li>
          <li class="nav-item">
            <a href="/posts/">Archive</a>
          </li>
          <li class="nav-item">
            <a href="/tags/">Tags</a>
          </li>
          <li class="nav-item">
            <a href="/about/">About</a>
          </li>
      </ul>
    </header>

    <main class="tmpl-post">
      <h1>PSV3 Soldering Iron Hack - Part 2</h1>

<time datetime="2023-08-03">03 Aug 2023</time>
    <a href="/tags/reverse-engineering/" class="post-tag">reverse engineering</a>
    <a href="/tags/soldering-iron/" class="post-tag">soldering iron</a>
    <a href="/tags/stm32/" class="post-tag">stm32</a>

<p>This is the sequel to <a href="/posts/2023-07-26/psv3-soldering-iron-hack-part-1/">PSV3 Soldering Iron Hack - Part 1</a> where I already introduced the result of this whole project: <a href="https://github.com/spezifisch/T12PenSolder" target="_blank" rel="noopener noreferrer external">a quite usable replacement firmware</a>. Now let's see how we got there (so I guess it's rather a <em>prequel</em>).</p>
<p>I already showed my annotated board layout:</p>
<figure class="post-card post-card-shadow"><a href="/assets/img/solder-layout-Gu0HX3DsLr-orig.jpeg" title="Open image" target="_blank"><img alt="Photo of the PCB with highlighted and annotated traces" decoding="async" src="/assets/img/solder-layout-Gu0HX3DsLr-orig.jpeg" width="2338" height="365"></a><figcaption>Traced and annotated PCB layout, top layer (as always, click to enlarge).</figcaption></figure>
<p>It was really helpful to create this image to get a first level of understanding about what's going on in this soldering iron.
I made photos of the top and the bottom of the PCB,<sup class="footnote-ref"><a href="#fn1" id="fnref1">[1]</a></sup> added them as semi-transparent image layers in GIMP and traced the signals and parts in additional layers by hand/mouse.</p>
<p>Based on that image I created a <a href="/assets/posts/pen-solder/pen_solder_v3.pdf">reverse engineered schematic (PDF)</a> in KiCad, which has also been a handy reference while developing the new firmware. For purposes of documentation though in this post I believe a kind of top-down block diagram is a better starting point.</p>
<h2 id="soldering-tip" tabindex="-1">Soldering Tip <a class="direct-link" href="#soldering-tip" aria-hidden="true">#</a></h2>
<p>Let's start with a view of the parts around the soldering tip:</p>
<figure class="post-card post-card-shadow"><a href="/assets/img/solder-layout_blocks_front-xA1z78H_hj-orig.jpeg" title="Open image" target="_blank"><img alt="Photo of the PCB with highlighted blocks" decoding="async" src="/assets/img/solder-layout_blocks_front-xA1z78H_hj-orig.jpeg" width="1514" height="365"></a><figcaption>A closer view at the business end.</figcaption></figure>
<p>On the left you can see two contacts for the <strong>T12 soldering tip</strong> and connecting to those, the 20 V high-side heater driver MOSFET, and the operational amplifier for the tip's thermocouple temperature measurement.</p>
<figure class="post-card post-card-shadow"><a href="/assets/img/solder-t12tips-11Qzp1O-OE-orig.jpeg" title="Open image" target="_blank"><img alt="3 soldering tips in plastic bags" decoding="async" src="/assets/img/solder-t12tips-11Qzp1O-OE-orig.jpeg" width="943" height="478"></a><figcaption>My choice of T12-compatible soldering tips: T12-K (knife shape), T12-BC2 (beveled shape), T12-D24 (chisel shape)</figcaption></figure>
<p><a href="https://www.hakko.com/english/support/doc/result.php?s_cat%5B1%5D=1&amp;s_keyword=T12" target="_blank" rel="noopener noreferrer external">Hakko T12</a> compatible soldering tips are used by many cheap soldering irons and soldering stations (and by a few good ones). It's an <a href="https://eleshop.eu/knowledgebase/active-or-passive/" target="_blank" rel="noopener noreferrer external">active</a> soldering tip so the heat is generated close to the tip leading to fast heat up times. Temperature measurement is also already integrated using a thermocouple. Both are connected in series between two pins (here marked <em>-/+</em> while <em>E</em> is for Earth and not connected in this low voltage soldering iron):</p>
<figure class="post-card post-card-shadow post-card-darker"><a href="/assets/img/solder-tip-schema-9DuIDNRNZd-orig.jpeg" title="Open image" target="_blank"><img alt="Cross-section of the soldering tip" decoding="async" src="/assets/img/solder-tip-schema-9DuIDNRNZd-orig.jpeg" width="740" height="315"></a><figcaption>Inside a T12 soldering tip. Credit: Marius Taciuc</figcaption></figure>
<h3 id="heating" tabindex="-1">Heating <a class="direct-link" href="#heating" aria-hidden="true">#</a></h3>
<p>To heat the soldering tip we just apply our supply voltage to the pins (<em>-/+</em> in the image above, <em>TIP_A/B</em> in the schematic below).
This happens to always be 20 V in our case.</p>
<figure class="post-card post-card-shadow post-card-darker"><a href="/assets/img/solder-sch-driver-49uxwVF05Z-orig.png" title="Open image" target="_blank"><img alt="Schematic showing two MOSFETs driving the tip" decoding="async" src="/assets/img/solder-sch-driver-49uxwVF05Z-orig.png" width="1154" height="648"></a><figcaption>Soldering tip heating circuit</figcaption></figure>
<p>On the left we have a pin of the microcontroller (signal <em>TIPHEAT_DRV</em>) controlling a simple driver stage with a small N-MOSFET (<em>Q2</em> here, its marking is <code>72K</code>) which controls a P-MOSFET (<em>Q1</em> here) that switches 20 V (<em>VBUS</em>) going to the tip on or off.<sup class="footnote-ref"><a href="#fn2" id="fnref2">[2]</a></sup></p>
<p>During the heating phase we can model the soldering tip as a simple resistor. With a multimeter I measured its resistance to be ~8 Ohm (on my <em>T12-B2</em> tip).
So we can expect a theoretical maximum output of 50 W given the fixed supply voltage.</p>
<p>To regulate the heating element to get a stable temperature level we're switching the tip power with PWM to achieve a finer grained average output power. The desired output power is calculated by a PID controller (adapted from <a href="https://github.com/Ralim/IronOS" target="_blank" rel="noopener noreferrer external">IronOS</a>) that runs at 10 Hz which coincidentally is the same as the PWM frequency we're using.<sup class="footnote-ref"><a href="#fn3" id="fnref3">[3]</a></sup></p>
<h3 id="temperature-measurement" tabindex="-1">Temperature measurement <a class="direct-link" href="#temperature-measurement" aria-hidden="true">#</a></h3>
<p>Inside the tip there's a thermocouple connected in series with the heating element and thanks to the Seebeck effect, we can measure a voltage between the <em>-</em> and <em>+</em> pins which is related to the tip's temperature. But this works only when the heating element is turned off — otherwise you would just measure the 20 V that the power MOSFET connects to those pins.</p>
<p>Therefore we need to turn the heat off regularly for a fixed minimal amount of time; we need to wait for the temperature to stabilize a bit before taking ADC voltage samples. IronOS implements a bit more finesse at this point by filtering the measurements but it seems to work well enough to just increase the ADC sample time by a bit (like the stock firmware does, too).</p>
<figure class="post-card post-card-shadow post-card-darker"><a href="/assets/img/solder-sch-temperature-YPeiog0faX-orig.png" title="Open image" target="_blank"><img alt="Schematic showing an op amp circuit" decoding="async" src="/assets/img/solder-sch-temperature-YPeiog0faX-orig.png" width="1292" height="589"></a><figcaption>Thermocouple amplifier, from tip (left, TIP_A) to ADC input (right, TIPTEMP_MEAS).</figcaption></figure>
<p>With only e.g. ~11.3 mV at 450 °C the voltage of the thermocouple in our operating range is too small to measure directly with the ADC of our microcontroller. That's why we have a non-inverting amplifier op amp circuit to boost this voltage (e.g. to ~2.5 V at 450 °C).</p>
<h3 id="temperature-conversion" tabindex="-1">Temperature conversion <a class="direct-link" href="#temperature-conversion" aria-hidden="true">#</a></h3>
<p>Funnily enough there is <a href="https://hackaday.io/project/94905-hakko-revenge/log/144548-hakko-t12-thermocouple-is-not-type-k" target="_blank" rel="noopener noreferrer external">a bit</a> <a href="https://www.eevblog.com/forum/projects/measuring-temperature-from-a-t12-soldering-tip/" target="_blank" rel="noopener noreferrer external">of a</a> <a href="http://dangerousprototypes.com/forum/index.php?topic=5264.0" target="_blank" rel="noopener noreferrer external">disagreement</a> whether the type of the thermocouple in T12 tips is Type K, C, N, or whatever. I can't say which one is correct but either way it is going to be a linear temperature relationship between voltage and temperature in our operating range. So I made a round of rough measurements to get some input data for the PID controller.</p>
<figure class="post-card post-card-shadow"><a href="/assets/img/solder-calibvideo-cZs47e0Uuy-orig.jpeg" title="Open image" target="_blank"><img alt="Multimeter and soldering iron" decoding="async" src="/assets/img/solder-calibvideo-cZs47e0Uuy-orig.jpeg" width="1920" height="1080"></a><figcaption>Still from calibration video</figcaption></figure>
<p>I used the temperature mode of a <em>Voltcraft VC-523</em> multimeter, held it to the soldering tip, printed the raw ADC values in big numbers on the OLED display and filmed a video showing both the multimeter's and the iron's displays. Then I noted a bunch of temperature/ADC value pairs and did a linear regression in GNU Octave (<a href="/assets/posts/pen-solder/soldering_tip_linear_reg.m">soldering_tip_linear_reg.m</a>, <a href="/assets/posts/pen-solder/szf-tip1-calib1.txt">raw data</a>), which leads to the following graph with values converted to microvolts:</p>
<figure class="post-card post-card-shadow post-card-darker"><a href="/assets/img/solder-tipcalibration-jZOje2wGFu-orig.png" title="Open image" target="_blank"><img alt="Graph showing Temperature over Tip Voltage" decoding="async" src="/assets/img/solder-tipcalibration-jZOje2wGFu-orig.png" width="873" height="655"></a><figcaption>Quick and dirty first calibration run (blue: measurements, red: regression curve)</figcaption></figure>
<p>To implement my conversion function in the firmware I skipped the intermediate step and used ADC values directly to get the temperature in degrees Celsius with: <code>T/°C = 0.2037 * x/(ADC units) - 73.8</code></p>
<p>This worked for a start and using this I was able to get pretty consistent temperature control, i.e. I set the soldering iron to hold 320 °C and the multimeter agreed within a few degrees with that.</p>
<p>But there are a few problems to consider with my calibration:</p>
<ol>
<li>Specialized devices to calibrate soldering iron temperatures exist (e.g. search on Aliexpress for &quot;soldering iron calibration&quot;) — I don't know how using my multimeter's thermocouple compares to those.</li>
<li>It matters where on the tip you measure. I tried to hold the sensor in a consistent position on the side of the tip where the solder would be but results in a different place will not be the same. (Maybe <a href="https://hackaday.io/project/94905-hakko-revenge/log/144548-hakko-t12-thermocouple-is-not-type-k" target="_blank" rel="noopener noreferrer external">putting it in oil</a> would be better?)</li>
<li>I didn't consider cold junction compensation. So I probably have at least a fixed offset in my temperature control depending on the room temperature, thus I should probably subtract room temperature from the values. But then again this soldering iron doesn't have a separate sensor to measure ambient (or handle) temperature. Maybe this could be a setting in the GUI, though.</li>
</ol>
<p>Finally, comparing these results with <a href="https://github.com/Ralim/IronOS/blob/v2.21/source/Core/BSP/Pinecilv2/ThermoModel.cpp" target="_blank" rel="noopener noreferrer external">IronOS' temperature curve of a T12-style tip</a> they're pretty similar in the range I care about most (i.e. above 300 °C) except for a static offset, which may be due to my missing cold junction compensation or due to measuring errors on my side. On the low end (below 200 °C) there are bigger disagreements. In any case I would trust their measurements more than mine, so I'm going with their interpolation table instead of my regression function for now.</p>
<h2 id="usb-supply-control" tabindex="-1">USB supply control <a class="direct-link" href="#usb-supply-control" aria-hidden="true">#</a></h2>
<p>On the right half of the board the <em>CH224K Low-cost USB-PD Sink Controller</em> (<a href="https://www.laskakit.cz/user/related_files/ch224ds1.pdf" target="_blank" rel="noopener noreferrer external">datasheet</a>) runs the show and configures the USB power supply over the USB-C pins DM/DP/CC1/CC2. USB Bus Voltage <code>VBUS</code> then powers the soldering tip and it's also the input voltage for an <em>XL1509</em> 3.3 V buck regulator (<a href="https://www.xlsemi.com/datasheet/XL1509-EN.pdf" target="_blank" rel="noopener noreferrer external">datasheet</a>) that powers everything else on the board.</p>
<figure class="post-card post-card-shadow"><a href="/assets/img/solder-layout_blocks_back-Sk1gnLamgp-orig.jpeg" title="Open image" target="_blank"><img alt="PCB with ICs and USB-C jack" decoding="async" src="/assets/img/solder-layout_blocks_back-Sk1gnLamgp-orig.jpeg" width="792" height="365"></a><figcaption>Hardware blocks around the USB connector.</figcaption></figure>
<h3 id="usb-pd-controller" tabindex="-1">USB-PD controller <a class="direct-link" href="#usb-pd-controller" aria-hidden="true">#</a></h3>
<p>The <em>CH224K</em> gives two options to configure the voltage it requests from the USB power supply: A resistor connected to pin <code>CFG1</code> or high/low logic levels setting pins <code>CFG1/2/3</code>, which would be quite interesting if they were connected to a microcontroller.</p>
<figure class="post-card post-card-shadow post-card-darker"><a href="/assets/img/solder-pdoptions-xjLBAROc49-orig.png" title="Open image" target="_blank"><img alt="tables showing which resistor values or logic levels give which VUSB voltage" decoding="async" src="/assets/img/solder-pdoptions-xjLBAROc49-orig.png" width="847" height="496"></a><figcaption>CH224K voltage configuration options (source: datasheet)</figcaption></figure>
<p>Sadly, on this board the <code>CFGx</code> pins are not connected to anything, so 20 V is always chosen for <code>VUSB</code>. I'm thinking about connecting a 56 kΩ resistor to <code>CFG1</code> though to select 15 V and give the P-MOSFET a longer life.</p>
<h2 id="microcontroller" tabindex="-1">Microcontroller <a class="direct-link" href="#microcontroller" aria-hidden="true">#</a></h2>
<p>The microcontroller is an STM32 clone named <code>CHIPSEA 32F030P6F6</code>. It seems to behave pretty well and I didn't have any problems with the ADC or otherwise.</p>
<figure class="post-card post-card-shadow post-card-darker"><a href="/assets/img/solder-sch-uc-FCcV9O9xaP-orig.png" title="Open image" target="_blank"><img alt="A schematic showing one IC" decoding="async" src="/assets/img/solder-sch-uc-FCcV9O9xaP-orig.png" width="1343" height="747"></a><figcaption>Schematic around the microcontroller</figcaption></figure>
<p>This design has some annoying quirks though:</p>
<ul>
<li>The OLED I2C display isn't connected to the also available hardware I2C pins but to random I/O pins, so we have to use bit-banging to control the display.</li>
<li>The tip heat driver (<em>TIPHEAT_DRV</em> in my schematic) is connected to <code>PA3</code> which doesn't have PWM — the only timer channel on this F030 line is <em>TIM15_CH2</em> and this particular footprint doesn't have Timer 15.</li>
<li>There is no way to reset the &quot;STM32&quot;, e.g. for SWD debugging and also no UART/etc. output, so we need to test by pin wiggling until the display works</li>
</ul>
<h2 id="oled-display" tabindex="-1">OLED display <a class="direct-link" href="#oled-display" aria-hidden="true">#</a></h2>
<p>To find out how to talk with the OLED display I hooked up a logic analyzer to its I2C bus. As we can quickly see from all the I2C writes to address <code>0x3c</code> it probably uses a SSD1306-compatible protocol, so in the best case we only need to replicate the used initialization sequence and can then use the <a href="https://github.com/olikraus/u8g2" target="_blank" rel="noopener noreferrer external">u8g2 display library</a> to render our display content and do the rest of the communication.</p>
<figure class="post-card post-card-shadow"><a href="/assets/img/solder-layout_bottom_lcd-HvDugRhHtk-orig.jpeg" title="Open image" target="_blank"><img alt="An unpowered OLED display" decoding="async" src="/assets/img/solder-layout_bottom_lcd-HvDugRhHtk-orig.jpeg" width="1050" height="365"></a><figcaption>The bottom side of the PCB pretty much only contains this display (and the 3 buttons).</figcaption></figure>
<h3 id="bad-code" tabindex="-1">Bad code <a class="direct-link" href="#bad-code" aria-hidden="true">#</a></h3>
<p>I had already found the display initialization code in the dumped stock firmware (of course I could have also used the logic analyzer to dump it, but I didn't trust it due to the weird NACK behavior mentioned below):</p>
<figure class="post-card post-card-shadow post-card-darker"><a href="/assets/img/solder-ghidra-oled-QQS4TIBfC4-orig.png" title="Open image" target="_blank"><img alt="Pseudocode output from Ghidra" decoding="async" src="/assets/img/solder-ghidra-oled-QQS4TIBfC4-orig.png" width="438" height="823"></a><figcaption>In the stock firmware: First part of the OLED initialization</figcaption></figure>
<p>Funnily enough the stock firmware sends each of the bytes during the initialization in a separate I2C transmission (including start + stop condition, address byte, register byte) which takes needlessly long. You can just send the whole sequence in one transmission instead (and <em>u8g2</em> does this btw.).</p>
<p>Another weird behaviour is that, going by the logic analyzer traces, the display doesn't seem to send ACKs — after every byte the bus master (i.e. the microcontroller) should let go of the SDA line and the slave should pull it low to indicate acknowledgement of that byte.</p>
<figure class="post-card post-card-shadow"><a href="/assets/img/solder-la-i2c-khdtWWYgfy-orig.png" title="Open image" target="_blank"><img alt="" decoding="async" src="/assets/img/solder-la-i2c-khdtWWYgfy-orig.png" width="1182" height="360"></a><figcaption>Logic analyzer trace of I2C traffic with stock firmware: NACKs everywhere! (Red 'N' markers in the bottom row.)</figcaption></figure>
<p>It turns out that the reason why the OLED display doesn't seem to send ACKs is because it can't — the stock firmware leaves the I2C SDA and SDL pins in push-pull mode the whole time. This means the <em>high</em> state on SDA is actively driven high by the microcontroller. When the OLED display tries to drive it <em>low</em> during the ACK/NACK period, we get some in-between voltage on the SDA line that can be seen with an oscilloscope (which my logic analyzer decoded as <em>high</em>).<sup class="footnote-ref"><a href="#fn4" id="fnref4">[4]</a></sup></p>
<p>Also, as you can see in the initialization code above, they toggle <code>PA9</code> before starting the display initialization sequence. While this pin is probably meant to be hooked up to the display reset line, it isn't connected to anything on this board.</p>
<h3 id="better-code" tabindex="-1">Better code <a class="direct-link" href="#better-code" aria-hidden="true">#</a></h3>
<p>After some searching in the excellent <em>u8g2</em> sources (and a <em>lot</em> of trial &amp; error) I found the <a href="https://github.com/olikraus/u8g2/blob/08f70e80b44aa0673d6fde9901566ad3360923f6/csrc/u8x8_d_ssd1306_128x32.c#L41" target="_blank" rel="noopener noreferrer external">ssd1306 128x32 univision initialization</a> which is pretty much the same as the one used on our soldering iron (plus display inversion/rotation settings which are done elsewhere in the stock firmware).</p>
<p>The bit-banging driver was quite slow though on an STM32. One reason is that it uses Arduino I/O functions like <code>digitalWrite()</code> which seem to be very slow in <a href="https://github.com/stm32duino/Arduino_Core_STM32" target="_blank" rel="noopener noreferrer external">stm32duino</a> (there are some pin and port lookups which probably take a few cycles each time). Once I switched to using the <a href="https://github.com/stm32duino/Arduino_Core_STM32/blob/987519a166179e3ada198364f79d388d05f75d3b/cores/arduino/stm32/digital_io.h#L57" target="_blank" rel="noopener noreferrer external">low-level I/O functions</a> though I was able to get from ~26 kHz to ~89 kHz I2C clock speed.</p>
<p>With some further optimizations I finally got around 170 kHz. All together the whole display content is now rendered and transmitted in around 49 ms, which is well within the 100 ms PID controller and measurement period, so they don't interfere too much with each other (and due to the way I2C works, it shouldn't matter anyway if a clock cycle here or there is a bit longer).</p>
<h2 id="stock-firmware" tabindex="-1">Stock Firmware <a class="direct-link" href="#stock-firmware" aria-hidden="true">#</a></h2>
<p>I already showed in the previous post how to dump the stock firmware using an STLink USB stick.<sup class="footnote-ref"><a href="#fn5" id="fnref5">[5]</a></sup> For analysis in Ghidra I found <a href="https://github.com/Diverto/ghidra-stm32f0" target="_blank" rel="noopener noreferrer external">ghidra-stm32f0</a> quite useful — it's a loader that sets up memory regions and register names so you don't see reads and writes with addresses like <code>0x04001000</code> everywhere. (Be aware though that it labels many interrupt vector table entries that don't exist on our low-spec STM32F0 and are actually already code. Also it's missing a lot of timer and ADC peripherals that we're using but it's better than nothing.)</p>
<figure class="post-card post-card-shadow post-card-darker"><a href="/assets/img/solder-ghidra-main-MT8w6zvr_i-orig.png" title="Open image" target="_blank"><img alt="Pseudocode excerpt from Ghidra" decoding="async" src="/assets/img/solder-ghidra-main-MT8w6zvr_i-orig.png" width="595" height="803"></a><figcaption>Stock firmware main function: hardware setup, welcome screen, main screen</figcaption></figure>
<p>Initially I wanted to know how they're implementing their temperature controller but it wasn't that interesting anymore after the ported code from IronOS worked so well. There are always bits and pieces of display code or peripheral setup in between so the decompiled code is actually quite a slog to get through.</p>
<h2 id="custom-firmware" tabindex="-1">Custom Firmware <a class="direct-link" href="#custom-firmware" aria-hidden="true">#</a></h2>
<p>For my custom firmware I started with a <a href="https://github.com/spezifisch/T12PenSolder/tree/11f69184440a310940f4706372f33aca9d6d7e08/firmware/custom_variants/T12F030" target="_blank" rel="noopener noreferrer external">custom PlatformIO board</a> that I derived from an existing one using another MCU of the F030 line. A <a href="https://github.com/spezifisch/T12PenSolder/blob/11f69184440a310940f4706372f33aca9d6d7e08/firmware/boards/t12f030.json" target="_blank" rel="noopener noreferrer external">board</a> with this specific <code>STM32F030F6P6</code>  with 32 kB Flash and 4 kB RAM didn't exist so I had to create one. Peripheral initialization was a bit tricky because contrary to my expectations, <em>stm32duino</em> doesn't handle most of the clock setup of GPIOs/etc., so I ended up using <a href="https://www.st.com/en/ecosystems/stm32cube.html" target="_blank" rel="noopener noreferrer external">STM32Cube</a> to generate setup code for <a href="https://github.com/spezifisch/T12PenSolder/blob/11f69184440a310940f4706372f33aca9d6d7e08/firmware/custom_variants/T12F030/generic_clock.c" target="_blank" rel="noopener noreferrer external">the system clocks</a> and also the <a href="https://github.com/spezifisch/T12PenSolder/blob/11f69184440a310940f4706372f33aca9d6d7e08/firmware/src/cube_init.cpp" target="_blank" rel="noopener noreferrer external">ADC and GPIO setup</a> for me.</p>
<p>The core of my custom firmware is pretty simple: UI stuff (buttons + display) happens in the Arduino <code>loop()</code> function every 100 ms:</p>
<figure class="post-card post-card-shadow post-card-darker post-card-bgwhite"><a href="/assets/img/solder-dia-loop-TYYfVepz1d-orig.svg" title="Open image" target="_blank"><img alt="flow diagram" decoding="async" src="/assets/img/solder-dia-loop-TYYfVepz1d-orig.svg" width="397" height="397"></a><figcaption>Main loop routine</figcaption></figure>
<p>And soldering tip control is interrupt-driven by Timer 3 (<code>TIM3</code>), which resets every 100 ms (that interrupt turns the heat on) and is using 3 <em>compare interrupts</em> (1. heat off, 2. temperature measurement, 3. PID calculation):</p>
<figure class="post-card post-card-shadow post-card-darker post-card-bgwhite"><a href="/assets/img/solder-dia-timer3-owzNOZ0uZs-orig.svg" title="Open image" target="_blank"><img alt="timing diagram" decoding="async" src="/assets/img/solder-dia-timer3-owzNOZ0uZs-orig.svg" width="1531" height="425"></a><figcaption>TIM3 Interrupt Timing: Heating PWM, temperature measurement, PID update</figcaption></figure>
<p>The time at which the heat turns off is given by the desired duty cycle that the PID routine calculates. As discussed before, after turning off the heat we wait a fixed time for the temperature reading to stabilize (currently 10 ms, but this could be made shorter), and then take the ADC temperature reading.</p>
<p>In the current state the GUI is pretty simple. It only consists of one screen, you can press <em>SET</em> to turn the soldering iron on or off, then it goes to stand-by after 60 s. And you can set the target temperature using the <em>+</em> and <em>-</em> buttons from 150 to 450 °C.</p>
<figure class="post-card post-card-shadow"><a href="/assets/img/solder-heating-xsb48Wa5Ix-orig.jpeg" title="Open image" target="_blank"><img alt="Soldering iron display" decoding="async" src="/assets/img/solder-heating-xsb48Wa5Ix-orig.jpeg" width="4000" height="3000"></a><figcaption>Custom firmware on full power on the reassembled soldering iron, SWD wires peeking out.</figcaption></figure>
<p>Of course, as already noted, I was able to take the PID controller code from IronOS with a few little changes and it gives really good results without even tweaking the parameters (using the ones from the Pinecil V2).</p>
<h2 id="results-and-further-improvements" tabindex="-1">Results and further improvements <a class="direct-link" href="#results-and-further-improvements" aria-hidden="true">#</a></h2>
<p>I'm reasonably happy with the firmware at this point. It takes around 15 seconds to get from room temperature to 320 °C (and this, according to my limited measurement capabilities, really is 320 °C), while the stock firmware was way off with its temperature control, giving only around 280 °C when 320 °C are set. Temperature control during soldering is also rock solid.</p>
<p>Additionally, it might be a good idea to connect the USB-PD controller to a resistor to select a USB voltage of 15 V in order to improve the life-time of the P-MOSFET which is currently operating at its limit <code>V_GSmax</code> of 20 V. But this would come at the cost of longer heat-up times. Alternatively, you could replace that MOSFET with one of the few that withstand a <code>V_GS</code> of 25 or even 30 V.</p>
<p>Furthermore, cold junction compensation is currently being ignored and we can't probably do much without modifying the hardware. But we should at least be able to configure a fixed temperature calibration offset in the GUI like in IronOS.</p>
<p>Finally, it would be nice to get a soldering iron temperature calibration tool to make a more trustworthy calibration curve.</p>
<h3 id="links" tabindex="-1">Links <a class="direct-link" href="#links" aria-hidden="true">#</a></h3>
<ul>
<li><a href="/posts/2023-07-26/psv3-soldering-iron-hack-part-1/">Flashing Instructions</a> (previous post)</li>
<li><a href="https://github.com/spezifisch/T12PenSolder" target="_blank" rel="noopener noreferrer external">Firmware</a></li>
<li><a href="https://github.com/spezifisch/T12PenSolder/blob/main/pen_solder_v3.pdf" target="_blank" rel="noopener noreferrer external">Schematic</a></li>
</ul>
<section class="footnotes">
<h2 class="mt-3" id="footnotes" tabindex="-1">Footnotes <a class="direct-link" href="#footnotes" aria-hidden="true">#</a></h2>
<ol class="footnotes-list">
<li id="fn1" class="footnote-item"><p>The bottom side of the PCB isn't very interesting so I'm skipping it here. You can <a href="https://github.com/spezifisch/T12PenSolder/blob/11f69184440a310940f4706372f33aca9d6d7e08/layout_bottom.jpg" target="_blank" rel="noopener noreferrer external">look at it here</a>. It pretty much only contains 3 buttons, the OLED display (we don't need to know more than that it's connected by I2C), and a small N-MOSFET driving the bigger P-MOSFET that switches the heat. <a href="#fnref1" class="footnote-backref">↩︎</a></p>
</li>
<li id="fn2" class="footnote-item"><p>The N-FET is needed because the microcontroller can't switch the gate of Q1 directly. <a href="#fnref2" class="footnote-backref">↩︎</a></p>
</li>
<li id="fn3" class="footnote-item"><p>IronOS implements another trick to get faster heat up times, but we don't (yet): They're using a lower 5 Hz PID rate and PWM frequency during the initial heating phase to get higher power output by allowing for a higher PWM duty cycle. The duty cycle can be higher because overhead of the time needed for temperature measurement to settle is constant (see next section). The trade-off for this higher output power is less accurate temperature control. <a href="#fnref3" class="footnote-backref">↩︎</a></p>
</li>
<li id="fn4" class="footnote-item"><p>The proper way to do handle this would be for the microcontroller to either configure the pin as open-drain (instead of push-pull) or to switch the pin from <em>output</em> to <em>input</em>. The latter one is also useful if you actually want to know what the slave responded. <a href="#fnref4" class="footnote-backref">↩︎</a></p>
</li>
<li id="fn5" class="footnote-item"><p>I haven't yet decided whether I want to publish that dump but if you're interested in (probably also shoddy?) soldering station firmware dumps, you can find some at <a href="https://github.com/deividAlfa/stm32_soldering_iron_controller/tree/master/Original_FW" target="_blank" rel="noopener noreferrer external">https://github.com/deividAlfa/stm32_soldering_iron_controller/tree/master/Original_FW</a>. <a href="#fnref5" class="footnote-backref">↩︎</a></p>
</li>
</ol>
</section>

        <hr>
        <ul>
                <li>Next: <a href="/posts/2024-09-21/simple-doorbell-detection-and-remote-triggering-using-esphome/">Simple doorbell detection and remote triggering using ESPHome</a>
                </li>
            
                <li>Previous: <a href="/posts/2023-07-26/psv3-soldering-iron-hack-part-1/">PSV3 Soldering Iron Hack - Part 1</a>
                </li>
            
        </ul>
    </main>

    <footer>
        <ul class="footer-buttons">
            <li>
                <a href="/feed/feed.xml">
                    <svg fill="currentColor" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                        <title>Feed</title>
                        <path d="M14 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H2a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h12zM2 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"></path><path d="M5.5 12a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm-3-8.5a1 1 0 0 1 1-1c5.523 0 10 4.477 10 10a1 1 0 1 1-2 0 8 8 0 0 0-8-8 1 1 0 0 1-1-1zm0 4a1 1 0 0 1 1-1 6 6 0 0 1 6 6 1 1 0 1 1-2 0 4 4 0 0 0-4-4 1 1 0 0 1-1-1z"></path>
                    </svg>
                </a>
            </li>
            <li>
                <a href="/sitemap.xml">
                    <svg fill="currentColor" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
                        <title>Sitemap</title><path fill-rule="evenodd" d="M6 3.5A1.5 1.5 0 0 1 7.5 2h1A1.5 1.5 0 0 1 10 3.5v1A1.5 1.5 0 0 1 8.5 6v1H11a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-1 0V8h-5v.5a.5.5 0 0 1-1 0v-1A.5.5 0 0 1 5 7h2.5V6A1.5 1.5 0 0 1 6 4.5v-1zM8.5 5a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1zM3 11.5A1.5 1.5 0 0 1 4.5 10h1A1.5 1.5 0 0 1 7 11.5v1A1.5 1.5 0 0 1 5.5 14h-1A1.5 1.5 0 0 1 3 12.5v-1zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1zm4.5.5a1.5 1.5 0 0 1 1.5-1.5h1a1.5 1.5 0 0 1 1.5 1.5v1a1.5 1.5 0 0 1-1.5 1.5h-1A1.5 1.5 0 0 1 9 12.5v-1zm1.5-.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-1z"></path></svg>
                </a>
            </li>
        </ul>
    </footer>

    <!-- Current page: /posts/2023-08-03/psv3-soldering-iron-hack-part-2/ -->
  </body>
</html>